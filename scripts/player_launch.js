/*
 * LaPhunk production STRATUS PLAYER INITIALIZATION.
 * 
 *   This file is part of LaPhunk production website.
 * 
 *   You can redistribute it and/or modify it under the terms of the
 *   GNU Lesser General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.
 * 
 *   This code is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 * 
 *   See <http://www.gnu.org/licenses/> for complete copy of the GNU
 *   Lesser General Public License.
 *
 * @author Nicolas Broussard <laphunkprod@gmail.com>
 * (C) LaPhunk, 2013.
 */

$(document).ready(function(){
  $.stratus({
      links:      'http://soundcloud.com/laphunk/sets/laphunk',
      random:     false,
      user:       false,
      stats:      false,
      buying:     false,
      download:   false,
      volume:     70,
      theme:      'http://35.198.79.250/laphunk/style/modules/player.css',
      /*theme:      'themes/dark.css',*/
      color:      'ffb088'
  });
});
