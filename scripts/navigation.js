/*
 * LaPhunk production NAVIGATION PLUGIN INITIALIZATION.
 * 
 *   This file is part of LaPhunk production website.
 * 
 *   You can redistribute it and/or modify it under the terms of the
 *   GNU Lesser General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.
 * 
 *   This code is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 * 
 *   See <http://www.gnu.org/licenses/> for complete copy of the GNU
 *   Lesser General Public License.
 *
 * @author Nicolas Broussard <laphunkprod@gmail.com>
 * (C) LaPhunk, 2013.
 */

var matrix = [
    [
        {full:0},{full:1,moveDirection:'yx'},{full:0}
    ],
    [
        {full:1,moveDirection:'xy'},{full:1,first:true,moveDirection:'yx'},{full:1,moveDirection:'xy'}
    ],
    [
        {full:1,moveDirection:'xy'},{full:1,moveDirection:'yx'},{full:1,moveDirection:'xy'}
    ]
];

$(document).ready(function() {
   $(".page").ferroSlider({
      ajaxLoading             : true,
      ajaxScript              : 'http://35.198.79.250/laphunk/scripts/load_page.php',
      displace                : matrix,
      createMap               : true,
      mapPosition             : '84%_right',
      time                    : 500
   });
});

function navTo(url) {
   $.fn.ferroSlider.slideTo(url);
   document.title = 'LaPhunk prod. — ' + url.toUpperCase() ;
}
