/*
 * LaPhunk production CUSTOM IMAGE ZOOM SCRIPT.
 * 
 *   This file is part of LaPhunk production website.
 * 
 *   You can redistribute it and/or modify it under the terms of the
 *   GNU Lesser General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.
 * 
 *   This code is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 * 
 *   See <http://www.gnu.org/licenses/> for complete copy of the GNU
 *   Lesser General Public License.
 *
 * @author Nicolas Broussard <laphunkprod@gmail.com>
 * (C) LaPhunk, 2013.
 */

$(".zoom_enabled").yoxview({
   autoHideMenu :            false,
   cacheImagesInBackground : false,
   lang :                    'fr',
   popupResizeTime :         200,
   renderInfoPin :           false,
   titleAttribute :          'alt',
   titleDisplayDuration :    3000, // FIXME !
   titlePadding :            30
});
